/*
 module SEXT10(
  input logic     [10:0] in_11, 
  output logic     [15:0] out_11 ); 

  always_comb 
  begin
    out_11 = { {5{in_11[10]}}, in_11}; // this just concatenate the value with 5 bit of in[10]
  end
 endmodule
  
 
  module SEXT8(
  input logic     [8:0] in_12, 
  output logic     [15:0] out_12 ); 

  always_comb 
  begin
    out_12  = { {7{in_12[8]}}, in_12}; // this just concatenate the value with 5 bit of in[10]
  end
 endmodule
 
 
 */
 /*    
  module SEXT5(
  input logic     [5:0] in_1, 
  output logic     [15:0] out_1 ); 

  always_comb 
  begin
    out_1 = { {10{in_1[5]}}, in_1}; // this just concatenate the value with 5 bit of in[10]
  end
 endmodule
 
  module SEXT4(
  input logic     [4:0] in_1, 
  output logic     [15:0] out_1 ); 

  always_comb 
  begin
    out_1 = { {11{in_1[4]}}, in_1}; // this just concatenate the value with 5 bit of in[10]
  end
 endmodule

 */