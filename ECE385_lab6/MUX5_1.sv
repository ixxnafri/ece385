module MUX5_1
		(input logic S0,S1,S2,S3,      // GatePC, GateMDR, GateMARMUX, GateALU/*, S1, S2, S3, */
		input logic [15:0] D0,D1,D2,D3,
		output logic[15:0] Y
		);
		
		logic [3:0] S; 
		
		always_comb
			begin
			S = {S3,S2,S1,S0};
			
				if (S == 4'b1000)
					Y = D3;
				else if (S == 4'b0100)
					Y = D2;
				else if (S == 4'b0010)
					Y = D1;
				else if (S == 4'b0001)
					Y = D0;	
				else  
					Y = 16'hZZZZ;
				
			end
		
endmodule
