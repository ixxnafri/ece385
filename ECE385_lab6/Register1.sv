module Register1		

				(input Clk, Reset, Load,
				input logic Din,
				output logic Dout);
				
			
			always_ff@(posedge Clk)
			begin
					if (Reset)
						Dout <= 1'b0;
						
					else if(Load)
						Dout <= Din;
					
					else
						Dout <= Dout;
			end
endmodule   