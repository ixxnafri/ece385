module Register_File(
							input Clk,
							input Reset,
							input [2:0] SR1_ADDR, SR2, DR,
							input [15:0] BUS_DATA,
							output [15:0] SR1_OUTPUT, SR2_OUTPUT);
	


logic [15:0] reg_array [8]; //four 16-bits registers
 
 
//sr1_out and sr2_out are outputs
assign SR1_OUTPUT = reg_array[SR1_ADDR];
assign SR2_OUTPUT = reg_array[SR2];
 
always_ff @ (posedge Clk)
begin
    if(Reset) begin
       for(integer i = 0; i < 8; i = i + 1)
       begin
          reg_array[i] <= 16'b0;
       end
    end
 
    else begin
         reg_array[DR] <= BUS_DATA;
    end
end


endmodule
