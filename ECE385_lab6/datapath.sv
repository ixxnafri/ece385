 module datapath( 
		input logic Clk, Reset,
		input logic LD_PC, LD_IR, LD_MDR, LD_MAR,LD_CC, LD_BEN, LD_REG,
		input logic [1:0] PCMUX, ADDR2MUX,	ALUK,		
		input logic GatePC, GateMDR, GateMARMUX, GateALU,
		input logic MIO_EN, SR1MUX, DRMUX, SR2MUX, ADDR1MUX, 
		input logic [15:0] DATA_TO_CPU,
		output logic [15:0] MAR_OUTPUT,
		output logic [15:0] IR_OUTPUT,
		output logic [15:0] MDR_OUTPUT,
		output logic [15:0] PC_OUTPUT, BUS_OUTPUT, ALU_OUTPUT,
//		output logic [15:0] ADDR1MUX_OUTPUT, ADDR2MUX_OUTPUT,ADDR_OUTPUT, IR_SEXT4, IR_SEXT5, IR_SEXT8, IR_SEXT10, SR2MUX_OUTPUT, 
//		output logic [15:0] SR1_OUTPUT, SR2_OUTPUT,
//		output logic [2:0] SR1MUX_OUTPUT, DRMUX_OUTPUT,
		//output logic [15:0] R0,R1,R2,R3,R4,R5,R6,R7,
		output logic BEN
		);
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
16 Bit Registers are placed here
-PC Register
-Mar Register
-MDR Register
-IR Register
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							

		logic [15:0] PCMUX_OUTPUT,
						 PREMDR_OUTPUT,
						 DATA_FROM_CPU;
						
		logic [15:0] MDR_OUTPUT_L, PC_OUTPUT_L;
		logic [15:0] ADDR1MUX_OUTPUT, ADDR2MUX_OUTPUT,ADDR_OUTPUT, IR_SEXT4, IR_SEXT5, IR_SEXT8, IR_SEXT10, SR2MUX_OUTPUT; 
		logic [15:0] SR1_OUTPUT, SR2_OUTPUT;
		logic [2:0] SR1MUX_OUTPUT, DRMUX_OUTPUT;
								
						
Register16  PC_REG(
				
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_PC),
							.Din(PCMUX_OUTPUT), 
							.Dout(PC_OUTPUT_L));
							
Register16 MAR_REG(
							
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_MAR),
							.Din(BUS_OUTPUT),
							.Dout(MAR_OUTPUT));
						
							
Register16 MDR_REG(
				
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_MDR),
							.Din(PREMDR_OUTPUT),
							.Dout(MDR_OUTPUT_L));
							
Register16 IR_REG(
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_IR),
							.Din(BUS_OUTPUT),
							.Dout(IR_OUTPUT));



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
This part below is where we put all our MUXes for Part 1
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							


MUX4_1	  PCMUX1(			//00 - PC + 1 //01 - ADDR // 10 - BUS. ASK THE TA. CONSIDER CHANGING IT
							.D0(PC_OUTPUT_L + 16'h0001), 
							.D1(ADDR_OUTPUT),
							.D2(BUS_OUTPUT),
							.D3(16'HZZZZ),
							.Select(PCMUX),
							.Y(PCMUX_OUTPUT)); //consider changing to zeros
							
MUX2_1    PREMDR_MUX(

							.D0(BUS_OUTPUT),
							.D1(DATA_TO_CPU),
							.Select(MIO_EN),
							.Y(PREMDR_OUTPUT));
						
MUX5_1    BUS(
							.S0(GatePC),
							.S1(GateMDR),
							.S2(GateALU),
							.S3(GateMARMUX),
							.D0(PC_OUTPUT_L),
							.D1(MDR_OUTPUT_L), // MDR
							.D2(ALU_OUTPUT), // ALU
							.D3(ADDR_OUTPUT), // MARMUX
							.Y(BUS_OUTPUT)); 

assign PC_OUTPUT = PC_OUTPUT_L;
assign MDR_OUTPUT = MDR_OUTPUT_L;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
This part below is where we put all our MUXes for Part 2
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							


assign IR_SEXT4 = {{11{IR_OUTPUT[4]}},IR_OUTPUT[4:0]};
assign IR_SEXT5 = {{10{IR_OUTPUT[5]}},IR_OUTPUT[5:0]};
assign IR_SEXT8 = {{7{IR_OUTPUT[8]}},IR_OUTPUT[8:0]};
assign IR_SEXT10 = {{5{IR_OUTPUT[10]}},IR_OUTPUT[10:0]};
 
							
MUX4_1 ADDR2MUX1(    .D0(16'h0000), // 0000
							.D1(IR_SEXT5), //SEXT5 DATA
							.D2(IR_SEXT8), //SEXT8 DATA
							.D3(IR_SEXT10) ,//SEXT10 DATA
							.Select(ADDR2MUX), //2BIT SELECT FROM ISDU
							.Y(ADDR2MUX_OUTPUT)); // OUTPUT
							
MUX2_1 ADDR1MUX1(
							.D0(PC_OUTPUT_L),
							.D1(SR1_OUTPUT),
							.Select(ADDR1MUX), //2BIT SELECT FROM ISDU
							.Y(ADDR1MUX_OUTPUT));


												
assign  ADDR_OUTPUT  = ADDR1MUX_OUTPUT + ADDR2MUX_OUTPUT;
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
					
/*
DRMUX
SR1MUX
REG_FILE
SR2MUX 
ALU
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							

					

MUX2_1 #(3) DRMUX1 (	.D0(3'b111),
							.D1(IR_OUTPUT[11:9]),
							.Select(DRMUX),
							.Y(DRMUX_OUTPUT));
							
							
MUX2_1 #(3) SR1MUX1(	.D0(IR_OUTPUT[11:9]),
							.D1(IR_OUTPUT[8:6]),
							.Select(SR1MUX),
							.Y(SR1MUX_OUTPUT));
							
							
REG_FILE REG8 (		.Clk(Clk),
							.Reset(Reset),
							.LD_REG(LD_REG),
							.DRMUX_OUTPUT(DRMUX_OUTPUT),
							.SR1MUX_OUTPUT(SR1MUX_OUTPUT),
							.SR2(IR_OUTPUT[2:0]), 
							.BUS_OUTPUT(BUS_OUTPUT),
							.SR1_OUT(SR1_OUTPUT),
							.SR2_OUT(SR2_OUTPUT), 
							.*);
							
MUX2_1 SR2MUX1(		.D0(SR2_OUTPUT),
							.D1(IR_SEXT4),
							.Select(SR2MUX),
							.Y(SR2MUX_OUTPUT));
							
							
ALU 	  ALU1 (       .A(SR1_OUTPUT),
							.B(SR2MUX_OUTPUT),
							.Select(ALUK),
							.Y(ALU_OUTPUT));
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

logic NZP_OUTPUT; 


							
CC_module  CC_LOGIC( .Clk(Clk),
							.Reset(Reset),
							.LD_CC(LD_CC),
							.NZP119(IR_OUTPUT[11:9]),
							.BUS_OUTPUT(BUS_OUTPUT),
							.NZP_OUTPUT(NZP_OUTPUT));
							
Register1  BEN_REG ( .Clk(Clk),
							.Reset(Reset),
							.Load(LD_BEN),
							.Din(NZP_OUTPUT),
							.Dout(BEN));  // ALREADY SET UP, UP ABOVE
							
							
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
							
endmodule
	