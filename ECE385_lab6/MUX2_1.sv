module MUX2_1 #(parameter width = 16)
		(input logic[width-1:0] D0, D1,
		input logic Select,
		output logic[width-1:0] Y);
		
		always_comb
			begin
				if (Select)
					Y = D1;	
				else
					Y = D0;
				
			end
		
endmodule

