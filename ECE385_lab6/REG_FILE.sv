module REG_FILE ( input Clk, Reset, LD_REG,
						input logic [2:0] DRMUX_OUTPUT, SR1MUX_OUTPUT, SR2, //3 bits coz it can only go to 7
						input [15:0] BUS_OUTPUT, 
						output [15:0] SR1_OUT, SR2_OUT
						//,output logic [15:0] R0,R1,R2,R3,R4,R5,R6,R7
						);
						
		logic [15:0] Reg [8]; //8 x 16-bit registers
		assign SR1_OUT = Reg [SR1MUX_OUTPUT];
		assign SR2_OUT = Reg [SR2];
		
		always_ff @ (posedge Clk)
		begin 
			if (Reset) 
			begin
						Reg[0] <= 16'h0000;
						Reg[1] <= 16'h0000;
						Reg[2] <= 16'h0000;
						Reg[3] <= 16'h0000;
						Reg[4] <= 16'h0000;
						Reg[5] <= 16'h0000;
						Reg[6] <= 16'h0000;
						Reg[7] <= 16'h0000;
			end
			
			else if (LD_REG)
				begin
					Reg[DRMUX_OUTPUT] <= BUS_OUTPUT;
				end
			
			else
				begin
				Reg[DRMUX_OUTPUT] <= Reg[DRMUX_OUTPUT];
				end
			
		end
			
			
//			assign R0 = Reg[0];
//			assign R1 = Reg[1];
//			assign R2 = Reg[2];
//			assign R3 = Reg[3];
//			assign R4 = Reg[4];
//			assign R5 = Reg[5];
//			assign R6 = Reg[6];
//			assign R7 = Reg[7];
			
endmodule
