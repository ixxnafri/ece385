module testbench();

timeunit 10ns;	// Half clock cycle at 50 MHz
			// This is the amount of time represented by #1 
timeprecision 1ns;

logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7;
logic CE, UB, LB, OE, WE, TEST_GATEPC, TEST_DRMUX, TEST_SR1MUX,TEST_LD_REG; 
logic [19:0] ADDR;
wire [15:0] Data;
//logic [15:0] TEST_PC,TEST_MAR, TEST_MDR,TEST_IR,TEST_DATA_TO_CPU, TEST_ADDR2MUX_OUTPUT, TEST_ADDR_OUTPUT, TEST_SR1_OUTPUT, 
//TEST_SR2_OUTPUT, TEST_IR_SEXT4, TEST_IR_SEXT5, TEST_IR_SEXT8, TEST_IR_SEXT10, TEST_SR2MUX_OUTPUT, TEST_BUS_OUTPUT, TEST_ALU_OUTPUT, TEST_ADDR1MUX_OUTPUT;
logic [15:0] S;
logic Clk = 0;
logic Reset, Run, Continue;
logic [11:0] LED;
logic [2:0] TEST_SR1MUX_OUTPUT, TEST_DRMUX_OUTPUT;
//logic [15:0] R0,R1,R2,R3,R4,R5,R6,R7;

				
lab6_toplevel slc3(.*);	


always begin : CLOCK_GENERATION
#1 Clk = ~Clk;
end

initial begin: CLOCK_INITIALIZATION
    Clk = 0;
end 


initial begin: TEST_VECTORS
Reset = 1;	
Run = 1;
Continue = 1;

#2 Reset = 0;
#2 Reset = 1;

#2 Run = 0;
#2 Run = 1;


#22 Continue = 0;
#2  Continue = 1;

#22 Continue = 0;
#2  Continue = 1;
//
//#20 Continue = 0;
//#2 Continue = 1;
//
//#20 Continue = 0;
//#2 Continue = 1;
//
//#20 Continue = 0;
//#2 Continue = 1;
//#1 Reset = 0;
//#2 Reset = 1;

end

endmodule
