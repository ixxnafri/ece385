module CC_module (
		
						input Clk, Reset, LD_CC,
						input [2:0] NZP119,
						input [15:0] BUS_OUTPUT,
						output logic NZP_OUTPUT);
		logic N,Z,P;
		
		always_ff @(posedge Clk) 
			begin
			NZP_OUTPUT <= 1'b0; 
			if (LD_CC)
			begin
				if(BUS_OUTPUT[15] == 1'b1)
					begin
					N <= 1'b1;
					Z <= 1'b0;
					P <= 1'b0;
					end
				else if (BUS_OUTPUT == 16'h0000)
					begin
					N <= 1'b0;
					Z <= 1'b1;
					P <= 1'b0;
					end
				else
					begin
					N <= 1'b0;
					Z <= 1'b0;
					P <= 1'b1;
					end
			end
			
			
			if(((NZP119[2] == 1'b1) && (N == 1'b1)) || ((NZP119[1] == 1'b1) && (Z == 1'b1))  || ((NZP119[0] == 1'b1) && (P == 1'b1)) )
				begin
				NZP_OUTPUT <= 1'b1;
				end
			end
			endmodule
			