//-------------------------------------------------------------------------
//      lab6_toplevel.sv                                                 --
//                                                                       --
//      Created 10-19-2017 by Po-Han Huang                               --
//                        Spring 2018 Distribution                       --
//                                                                       --
//      For use with ECE 385 Experment 6                                 --
//      UIUC ECE Department                                              --
//-------------------------------------------------------------------------
module lab6_toplevel( input logic [15:0] S,
                      input logic Clk, Reset, Run, Continue,
                      output logic [11:0] LED,
                      output logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
                      output logic CE, UB, LB, OE, WE, TEST_GATEPC, TEST_DRMUX, TEST_SR1MUX, TEST_LD_REG,
                      output logic [19:0] ADDR,
//							 output logic [15:0] TEST_PC,TEST_MAR,TEST_MDR,TEST_IR,TEST_DATA_TO_CPU, TEST_ADDR2MUX_OUTPUT, 
//							 TEST_ADDR_OUTPUT, TEST_SR1_OUTPUT, TEST_SR2_OUTPUT,TEST_IR_SEXT4, TEST_IR_SEXT5, TEST_IR_SEXT8, TEST_IR_SEXT10,
//							 TEST_SR2MUX_OUTPUT, TEST_ADDR1MUX_OUTPUT, TEST_BUS_OUTPUT, TEST_ALU_OUTPUT,
							 output logic [2:0] TEST_SR1MUX_OUTPUT, TEST_DRMUX_OUTPUT,
//							 output logic [15:0] R0,R1,R2,R3,R4,R5,R6,R7,
                      inout wire [15:0] Data);

slc3 my_slc(.*);
// Even though test memory is instantiated here, it will be synthesized into 
// a blank module, and will not interfere with the actual SRAM.
// Test memory is to play the role of physical SRAM in simulation.
test_memory my_test_memory(.Reset(~Reset), .I_O(Data), .A(ADDR), .*);

endmodule