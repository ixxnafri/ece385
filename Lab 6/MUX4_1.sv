module MUX4_1
		(input logic[15:0] D0, D1, D2, D3,
		input logic [1:0] Select,
		output logic[15:0] Y);
		
		always_comb
		begin
			
			case(Select)
			2'b11:
					Y = D3; 
					
         2'b10:
					Y = D2;
					
		   2'b01:
					Y = D1;	
					
         2'b00:
					Y = D0;
		endcase
	end
		
endmodule

