 module SEXT10(
  input      [10:0] in, 
  output     [15:0] out ); 

  always_comb 
  begin
    out = { {5{in[10]}}, in}; // this just concatenate the value with 5 bit of in[10]
  end
 endmodule
 
 
 
 module SEXT8(
  input      [8:0] in, 
  output     [15:0] out ); 

  always_comb 
  begin
    out = { {7{in[8]}}, in}; 
  end
 endmodule
 
 
 module SEXT5(
  input      [5:0] in, 
  output     [15:0] out ); 

  always_comb 
  begin
    out = { {10{in[5]}}, in}; 
  end
 endmodule
 
 
 module SEXT4(
  input      [4:0] in, 
  output     [15:0] out ); 

  always_comb 
  begin
    out = { {11{in[4]}}, in}; 
  end
 endmodule
 
 
 