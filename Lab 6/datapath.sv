module datapath( 
		input logic Clk, Reset,
		input logic LD_PC, LD_IR, LD_MDR, LD_MAR,
		input logic [1:0] PCMUX, ADDR2MUX,			
		input logic GatePC, GateMDR, GateMARMUX, GateALU,
		input logic MIO_EN,
		input logic [15:0] DATA_TO_CPU,
		output logic [15:0] MAR_OUTPUT,
		output logic [15:0] IR_OUTPUT,
		output logic [15:0] MDR_OUTPUT,
		output logic [15:0] PC_OUTPUT
		);
		
		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
16 Bit Registers are placed here
-PC Register
-Mar Register
-MDR Register
-IR Register
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							

		logic [15:0] MDR_IN, 
						PCMUX_OUTPUT,
						BUS_OUTPUT,
						ALU_OUTPUT,
						PREMDR_OUTPUT,
						DATA_FROM_CPU,
						ADDR_OUTPUT;
		logic [15:0] MDR_OUTPUT_L, PC_OUTPUT_L;
						
						
Register16  PC_REG(
				
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_PC),
							.Din(PCMUX_OUTPUT), 
							.Dout(PC_OUTPUT_L));
							
Register16 MAR_REG(
							
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_MAR),
							.Din(BUS_OUTPUT),
							.Dout(MAR_OUTPUT));
						
							
Register16 MDR_REG(
				
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_MDR),
							.Din(PREMDR_OUTPUT),
							.Dout(MDR_OUTPUT_L));
							
Register16 IR_REG(
							.Clk(Clk),
							.Reset(Reset),
							.Load(LD_IR),
							.Din(BUS_OUTPUT),
							.Dout(IR_OUTPUT));



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
This part below is where we put all our MUXes for Part 1
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							


MUX4_1	  PCMUX1(			//00 - PC + 1 //01 - ADDR // 10 - BUS. ASK THE TA. CONSIDER CHANGING IT
							.D0(PC_OUTPUT_L + 16'h0001), 
							.D1(ADDR_OUTPUT),
							.D2(BUS_OUTPUT),
							.D3(PC_OUTPUT_L + 16'h0001),
							.Select(PCMUX),
							.Y(PCMUX_OUTPUT)); //consider changing to zeros
							
MUX2_1    PREMDR_MUX(

							.D0(BUS_OUTPUT),
							.D1(DATA_TO_CPU),
							.Select(MIO_EN),
							.Y(PREMDR_OUTPUT));
							
MUX5_1    BUS(
							.S0(GatePC),
							.S1(GateMDR),
							.S2(GateALU),
							.S3(GateMARMUX),
							.D0(PC_OUTPUT_L),
							.D1(MDR_OUTPUT_L), // MDR
							.D2(ALU_OUTPUT), // ALU
							.D3(ADDR_OUTPUT), // MARMUX
							.Y(BUS_OUTPUT)); 

assign PC_OUTPUT = PC_OUTPUT_L;
assign MDR_OUTPUT = MDR_OUTPUT_L;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
This part below is where we put all our MUXes for Part 2
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							

/*
SEXT5_OUTPUT
SEXT8_OUTPUT
SEXT10_OUTPUT
ADDR2MUX_OUTPUT
ADDR1MUX_OUTPUT

ADDR1MUX
ADDR2MUX
 
 */
 
							
//MUX4_1 ADDR2MUX(     .D0(16'h0000), // 0000
//							.D1(SEXT5_OUTPUT), //SEXT5 DATA
//							.D2(SEXT8_OUTPUT), //SEXT8 DATA
//							.D3(SEXT10_OUTPUT),//SEXT10 DATA
//							.Select(ADDR2MUX), //2BIT SELECT FROM ISDU
//							.Y(ADDR2MUX_OUTPUT)); // OUTPUT
//							
//MUX2_1 ADDR1MUX(
//							.D0(PC_OUTPUT),
//							.D1(SR1_OUTPUT),
//							.Select(ADDR1MUX), //2BIT SELECT FROM ISDU
//							.Y(ADDR1MUX_OUTPUT));
//							
//							
//assign ADDR_OUTPUT = ADDR1MUX_OUTPUT + ADDR2MUX_OUTPUT;
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
SEXT FILES
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							

//
//SEXT10 SEXT_10(      .input(IR_OUTPUT[10:0]),     // 11 BIT
//							.output(SEXT10_OUTPUT));
//							
//SEXT8  SEXT_8(       .input(IR_OUTPUT[8:0]),      // 9 BIT
//							.output(SEXT8_OUTPUT));
//							
//SEXT5  SEXT_5(			.input(IR_OUTPUT[5:0]),      // 6 BIT
//							.output(SEXT5_OUTPUT));							
//							
//SEXT4  SEXT_4(			.input(IR_OUTPUT[4:0]),      // 5 BIT
//							.output(SEXT4_OUTPUT));	
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
DRMUX
SR1MUX
REG_FILE
SR2MUX 
ALU
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							


/*
DRMUX
SR1MUX
SR2MUX
LD_REG
ALUK

DRMUX_OUTPUT
SR1MUX_OUTPUT
SR1_OUTPUT
SR2_OUTPUT
SR2MUX_OUTPUT */


//MUX2_1 #(3) DRMUX (	.D0(3'b111),
//							.D1(IR_OUTPUT[11:9]),
//							.select(DRMUX),
//							.Y(DRMUX_OUTPUT));
//							
//							
//MUX2_1 #(3) SR1MUX(	.D0(IR_OUTPUT[11:9]),
//							.D1(IR_OUTPUT[8:6]),
//							.Select(SR1MUX),
//							.Y(SR1MUX_OUTPUT));
//							
//							
//REG_FILE REG8 (		.Clk(Clk),
//							.Reset(Reset),
//							.LD_REG(LD_REG),
//							.DRMUX_IN(DRMUX_OUTPUT),
//							.SR1MUX_IN(SR1MUX_OUTPUT),
//							.SR2(IR_OUTPUT[2:0]), // CHECK WITH TA
//							.DIN(BUS_OUTPUT),
//							.SR1_OUTPUT(SR1_OUTPUT),
//							.SR2_OUTPUT(SR2_OUTPUT));
//							
//MUX2_1 SR2MUX(			.D0(SR2_OUTPUT),
//							.D1(SEXT4_OUTPUT),
//							.Select(SR2MUX),
//							.Y(SR2MUX_OUTPUT));
//							
//							
//ALU ALU(            	.A(SR1_OUTPUT),
//							.B(SR2MUX_OUTPUT),
//							.Select(ALUK),
//							.Y(ALU_OUTPUT));
							
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							
/*
Havent done yet 
- NZP FILE
- Branch Enable (BEN)
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////							



							
endmodule
	