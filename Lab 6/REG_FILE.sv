/*module REG_FILE ( input Clk, Reset, LD_REG,
						input logic [2:0] DRMUX_IN, SR1MUX_IN, SR2, //3 bits coz it can only go to 7
						input [15:0] Din, 
						output [15:0] SR1_OUTPUT, SR2_OUTPUT);
						
		logic [15:0] Reg [0:7]; //8 x 16-bit registers
		
		always_ff @ (posedge Clk)
		begin 
			if (Reset) 
			begin
					for(int i = 0; i < 8; i++)
					begin 
						Reg[i] = 16'h0000;
					end
			end
			else if (LD_REG)
					Reg[DRMUX_IN] = Din;
			end
			
			assign SR1_OUTPUT = data[SR1MUX_IN]
			assign SR2_OUTPUT = data[SR2];
endmodule
*/