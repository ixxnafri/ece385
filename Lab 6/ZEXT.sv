 module ZEXT20(
  input      [15:0] in, 
  output reg [19:0] out ); 

  always_comb 
  begin
    out = { {4{0}}, in}; // this just concatenate the value with 5 bit of in[10]
  end
 endmodule
 
 //we dont need this. its already done for us in slc3 line 72.